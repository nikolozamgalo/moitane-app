package com.lemondo.moitane.domain.shops

import com.lemondo.moitane.presentation.shop.ShopsListUIModel
import kotlinx.coroutines.flow.Flow

interface ShopsRepository {

    suspend fun get(): Flow<List<ShopsListUIModel>>

}
