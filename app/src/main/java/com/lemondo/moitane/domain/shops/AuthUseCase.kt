package com.lemondo.moitane.domain.shops

import com.lemondo.moitane.data.UserPreference
import com.lemondo.moitane.domain.login.AuthRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class AuthUseCase @Inject constructor(
    private val authRepository: AuthRepository,
    private val userPreference: UserPreference
) {

    suspend fun authenticate(): Flow<String> = flow {
        if (userPreference.getToken().isEmpty()) {
            authRepository.authenticate().collect {
                userPreference.put(it)
                emit(it)
            }
        } else emit(userPreference.getToken())
    }
}
