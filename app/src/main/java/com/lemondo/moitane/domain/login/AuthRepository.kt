package com.lemondo.moitane.domain.login

import kotlinx.coroutines.flow.Flow

interface AuthRepository {
    suspend fun authenticate(): Flow<String>
}
