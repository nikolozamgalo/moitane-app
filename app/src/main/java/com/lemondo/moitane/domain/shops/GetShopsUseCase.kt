package com.lemondo.moitane.domain.shops

import com.lemondo.moitane.presentation.shop.ShopsListUIModel
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.*
import javax.inject.Inject

class GetShopsUseCase @Inject constructor(
    private val authUseCase: AuthUseCase,
    private val shopsRepository: ShopsRepository
) {
    suspend fun get(): Flow<List<ShopsListUIModel>> {
        return authUseCase.authenticate().flatMapMerge {
            shopsRepository.get()
        }
    }
}
