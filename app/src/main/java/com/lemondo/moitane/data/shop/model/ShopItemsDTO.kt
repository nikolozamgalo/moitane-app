package com.lemondo.moitane.data.shop.model

import com.lemondo.moitane.presentation.shop.ShopsListUIModel
import com.lemondo.moitane.presentation.shop.WorkingHours
import kotlinx.serialization.Serializable

@Serializable
data class ShopItemsDTO(
    val id: Long,
    val name: String,
    val logoUrl: String,
    val backgroundUrl: String,
    val estimatedTime: String,
    val deliveryFee: Double,
    val averageRating: Double,
    val reviewsCount: Int,
    val workingHours: List<WorkingHoursDTO>,
    val isShopOpen: Boolean,
) {

    fun toUIModel() = ShopsListUIModel(
        id = id,
        name = name,
        deliveryFee = deliveryFee,
        averageRating = averageRating,
        reviewsCount = reviewsCount,
        logoUrl = logoUrl,
        backgroundUrl = backgroundUrl,
        estimatedTime = estimatedTime,
        workingHours = workingHours.map {
            WorkingHours(
                day = it.day,
                from = it.from,
                to = it.to,
                working = it.working
            )
        },
        isShopOpen = isShopOpen,
        onClick = {
            // TODO navigation handler
        }
    )

}
