package com.lemondo.moitane.data

data class ClientConfig(
    val baseURL: String,
    val readTimeOutSecs: Long,
    val connectTimeOutSecs: Long,
    val writeTimeOutSecs: Long,
)

