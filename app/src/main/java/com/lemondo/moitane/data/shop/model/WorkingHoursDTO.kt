package com.lemondo.moitane.data.shop.model

import kotlinx.serialization.Serializable

@Serializable
data class WorkingHoursDTO(
    val day: String,
    val from: String,
    val to: String,
    val working: Boolean
)
