package com.lemondo.moitane.data.shop.model

import kotlinx.serialization.Serializable

@Serializable
data class ShopsResponseDTO(
    val shops: List<ShopItemsDTO>,
    val httpStatusCode: Int,
    val userMessage: String?,
    val developerMessage: String?,
    val success: Boolean,
    val errors: List<String>?
)
