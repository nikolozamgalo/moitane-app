package com.lemondo.moitane.data

import com.lemondo.moitane.BuildConfig
import com.lemondo.moitane.data.login.model.AuthenticateResponseDTO
import com.lemondo.moitane.data.shop.model.ShopsResponseDTO
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

interface ApiService {

    @POST("/connect/token")
    @FormUrlEncoded
    suspend fun authenticate(
        @Field("grant_type") grantType: String = "client_credentials",
        @Field("scope") scope: String = BuildConfig.SCOPE,
        @Field("client_id") clientId: String = BuildConfig.CLIENT_ID,
        @Field("client_secret") clientSecret: String = BuildConfig.CLIENT_SECRET
    ): AuthenticateResponseDTO


    @GET("/v1/Shops")
    suspend fun getShops(): ShopsResponseDTO

}
