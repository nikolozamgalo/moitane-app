package com.lemondo.moitane.data.login

import com.lemondo.moitane.data.ApiService
import com.lemondo.moitane.data.UserPreference
import com.lemondo.moitane.domain.login.AuthRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class AuthRepositoryImpl @Inject constructor(
    private val apiService: ApiService,
) : AuthRepository {
    override suspend fun authenticate(): Flow<String> = flow {
        emit(apiService.authenticate().accessToken)
    }
}
