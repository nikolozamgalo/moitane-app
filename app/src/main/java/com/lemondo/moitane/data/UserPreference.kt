package com.lemondo.moitane.data

import android.content.SharedPreferences

class UserPreference(private val sharedPreferences: SharedPreferences) {

    fun getToken() = sharedPreferences.getString(ACCESS_TOKEN, "") ?: ""

    fun put(token: String) = sharedPreferences.edit().putString(ACCESS_TOKEN, token).apply()

    fun clear() = sharedPreferences.edit().clear().apply()

    companion object {
        const val ACCESS_TOKEN = "TOKEN"
    }
}
