package com.lemondo.moitane.data.shop

import com.lemondo.moitane.data.ApiService
import com.lemondo.moitane.domain.shops.ShopsRepository
import com.lemondo.moitane.presentation.shop.ShopsListUIModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class ShopsRepositoryImpl @Inject constructor(
    private val apiService: ApiService
) : ShopsRepository {

    override suspend fun get(): Flow<List<ShopsListUIModel>> = flow {
        emit(apiService.getShops().shops.map {
            it.toUIModel()
        })
    }
}
