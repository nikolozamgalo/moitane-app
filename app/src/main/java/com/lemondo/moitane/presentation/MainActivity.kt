package com.lemondo.moitane.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.lemondo.moitane.R
import com.lemondo.moitane.presentation.shop.ShopListAdapter
import com.lemondo.moitane.presentation.shop.ShopsViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val adapter = ShopListAdapter()
    private val viewModel: ShopsViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val shopsList: RecyclerView = findViewById(R.id.shopsList)
        shopsList.layoutManager = LinearLayoutManager(this)
        shopsList.adapter = adapter

        viewModel.items.observe(this, adapter::submitList)
        viewModel.error.observe(this, {
            MaterialAlertDialogBuilder(this)
                .setTitle(R.string.error)
                .setMessage(R.string.network_error_message)
                .show()
        })
    }
}
