package com.lemondo.moitane.presentation.shop

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.lemondo.moitane.R
import com.lemondo.moitane.databinding.ShopItemBinding

class ShopListAdapter :
    ListAdapter<ShopsListUIModel, ShopListAdapter.ViewHolder>(DiffUtilCallBack()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent)

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position) bindTo ShopItemBinding.bind(holder.itemView)
    }

    class ViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.shop_item, parent, false)
    )

    class DiffUtilCallBack : DiffUtil.ItemCallback<ShopsListUIModel>() {
        override fun areItemsTheSame(
            oldItem: ShopsListUIModel,
            newItem: ShopsListUIModel
        ) = oldItem == newItem

        override fun areContentsTheSame(
            oldItem: ShopsListUIModel,
            newItem: ShopsListUIModel
        ) = oldItem.id == newItem.id
    }
}
