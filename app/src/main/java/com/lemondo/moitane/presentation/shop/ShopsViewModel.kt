package com.lemondo.moitane.presentation.shop

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.lemondo.moitane.domain.shops.GetShopsUseCase
import com.lemondo.moitane.presentation.utils.SingleLiveEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ShopsViewModel @Inject constructor(
    private val getShopsUseCase: GetShopsUseCase
) : ViewModel() {

    val items = MutableLiveData<List<ShopsListUIModel>>()

    val error = SingleLiveEvent<Unit>()

    init {
        getShops()
    }

    private fun getShops() {
        viewModelScope.launch {
            getShopsUseCase.get().catch {
                error.call()
            }.collect {
                items.postValue(it)
            }
        }
    }
}
