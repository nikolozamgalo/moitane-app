package com.lemondo.moitane.presentation.shop

import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import com.lemondo.moitane.R
import com.lemondo.moitane.databinding.ShopItemBinding
import java.util.*

data class ShopsListUIModel(
    val id: Long,
    val name: String,
    val deliveryFee: Double,
    val averageRating: Double,
    val reviewsCount: Int,
    val logoUrl: String?,
    val estimatedTime: String,
    val backgroundUrl: String?,
    val workingHours: List<WorkingHours>?,
    val isShopOpen: Boolean,
    val onClick: () -> Unit
) {

    infix fun bindTo(binding: ShopItemBinding) {
        binding.estimatedTextView.text =
            binding.root.context.getString(
                R.string.shop_info,
                estimatedTime,
                deliveryFee.toString()
            )
        binding.shopNameTextView.text = name
        binding.reviewsCountTextView.text = reviewsCount.toString()
        binding.ratingBar.rating = averageRating.toFloat()
        binding.shopWorkingHoursTextView.text =
            formatDate(Calendar.getInstance().get(Calendar.DAY_OF_WEEK))
        binding.closeGroup.isVisible = isShopOpen

        binding.root.setOnClickListener { onClick.invoke() }

        Glide.with(binding.root.context)
            .load(backgroundUrl)
            .centerCrop()
            .into(binding.shopBackgroundImageView)

        Glide.with(binding.root.context)
            .load(logoUrl)
            .fitCenter()
            .into(binding.shopLogoImageView)
    }


    private fun formatDate(currentWeekDay: Int): String {
        workingHours?.get(currentWeekDay - 1)?.let {
            if (it.working)
                return "${it.from.formatDateTime()} - ${it.to.formatDateTime()}"
        }
        return ""
    }

    private fun String.formatDateTime(): String {
        val striped = this.split(":")
        return "${striped[0]}:${striped[1]}";
    }

}
