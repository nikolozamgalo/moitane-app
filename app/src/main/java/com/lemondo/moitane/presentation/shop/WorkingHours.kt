package com.lemondo.moitane.presentation.shop

data class WorkingHours(
    val day: String,
    val from: String,
    val to: String,
    val working: Boolean
)
