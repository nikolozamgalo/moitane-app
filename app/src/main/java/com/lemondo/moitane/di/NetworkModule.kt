package com.lemondo.moitane.di

import com.lemondo.moitane.BuildConfig
import com.lemondo.moitane.data.ApiService
import com.lemondo.moitane.data.ClientConfig
import com.lemondo.moitane.data.UserPreference
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Provides
    fun provideApiClient(retrofit: Retrofit): ApiService = retrofit.create(ApiService::class.java)

    @Provides
    fun getRetrofitInstance(
        okHttpClient: OkHttpClient,
        clientConfig: ClientConfig,
        converter: Converter.Factory,
    ): Retrofit = Retrofit.Builder()
        .client(okHttpClient)
        .baseUrl(clientConfig.baseURL)
        .addConverterFactory(converter)
        .build()

    @Provides
    fun providesAuthorizedOkHttpInstance(
        clientConfig: ClientConfig,
        userPreference: UserPreference
    ): OkHttpClient =
        OkHttpClient().newBuilder().run {
            readTimeout(clientConfig.readTimeOutSecs, TimeUnit.SECONDS)
            connectTimeout(clientConfig.connectTimeOutSecs, TimeUnit.SECONDS)
            writeTimeout(clientConfig.writeTimeOutSecs, TimeUnit.SECONDS)
            if (BuildConfig.DEBUG)
                addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            addInterceptor { chain ->
                val request = chain.request().newBuilder()
                    .addHeader("Content-Type", "application/json")
                    .addHeader("Authorization", "Bearer ${userPreference.getToken()}")
                chain.proceed(request.build())
            }.build()
            build()
        }

    @Provides
    fun provideClientConfig() = ClientConfig(
        baseURL = BuildConfig.SERVER_BASE_URL,
        readTimeOutSecs = BuildConfig.SERVER_TIME_OUT.toLong(),
        connectTimeOutSecs = BuildConfig.SERVER_TIME_OUT.toLong(),
        writeTimeOutSecs = BuildConfig.SERVER_TIME_OUT.toLong()
    )

}
