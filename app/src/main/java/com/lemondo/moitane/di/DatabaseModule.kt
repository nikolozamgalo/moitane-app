package com.lemondo.moitane.di

import android.content.Context
import com.lemondo.moitane.data.UserPreference
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DatabaseModule {

    @Provides
    @Singleton
    fun providesUserPreference(@ApplicationContext context: Context): UserPreference =
        UserPreference(context.getSharedPreferences("UserPreferences", Context.MODE_PRIVATE))

}
