package com.lemondo.moitane.di

import com.lemondo.moitane.data.login.AuthRepositoryImpl
import com.lemondo.moitane.data.shop.ShopsRepositoryImpl
import com.lemondo.moitane.domain.login.AuthRepository
import com.lemondo.moitane.domain.shops.ShopsRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Binds
    @Singleton
    abstract fun provideShopRepository(
        shopsRepository: ShopsRepositoryImpl
    ): ShopsRepository

    @Binds
    @Singleton
    abstract fun provideAuthRepository(
        authRepository: AuthRepositoryImpl
    ): AuthRepository

}
